#include <bits/stdc++.h>
using namespace std;
int main()
{
    list<string> browserHistory;
    string address;
    while (cin >> address && address != "end")
    {
        browserHistory.push_back(address);
    }

    int q;
    cin >> q;
    list<string>::iterator currentWord = browserHistory.begin();
    for (int i = 0; i < q; i++)
    {
        string word;
        cin >> word;
        if (word == "visit")
        {
            string newWord;
            cin >> newWord;
            auto it = find(browserHistory.begin(), browserHistory.end(), newWord);
            if (it != browserHistory.end())
            {
                currentWord = it;
                cout << *currentWord << endl;
            }
            else
                cout << "Not Available" << endl;
        }
        else if (word == "next")
        {
            if (next(currentWord) != browserHistory.end())
            {
                currentWord++;
                cout << *currentWord << endl;
            }
            else
            {
                cout << "Not Available" << endl;
            }
        }
        else if (word == "prev")
        {
            if (prev(currentWord) != browserHistory.end())
            {
                currentWord--;
                cout << *currentWord << endl;
            }
            else
                cout << "Not Available" << endl;
        }
    }

    // for (string str : browserHistory)
    // {
    //     cout << str << " ";
    // }
    return 0;
}