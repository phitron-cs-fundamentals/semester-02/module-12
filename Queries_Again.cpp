#include <bits/stdc++.h>
using namespace std;
class Node
{
public:
    int val;
    Node *next;
    Node *prev;
    Node(int val)
    {
        this->val = val;
        this->next = NULL;
        this->prev = NULL;
    }
};
void insert_head(Node *&head, Node *&tail, int val)
{
    Node *newNode = new Node(val);
    if (head == NULL)
    {
        head = newNode;
        tail = newNode;
        return;
    }
    newNode->next = head;
    head->prev = newNode;
    head = newNode;
}

void insert_tail(Node *&head, Node *&tail, int val)
{
    Node *newNode = new Node(val);
    if (tail == NULL)
    {
        head = newNode;
        tail = newNode;
        return;
    }
    tail->next = newNode;
    newNode->prev = tail;
    tail = newNode;
}
void print_left_to_right(Node *head)
{
    Node *tmp = head;
    while (tmp != NULL)
    {
        cout << tmp->val << " ";
        tmp = tmp->next;
    }
    cout << endl;
}

void print_reverse(Node *tail)
{
    Node *tmp = tail;
    while (tmp != NULL)
    {
        cout <<tmp->val << " ";
        tmp = tmp->prev;
    }
    cout <<endl;
}

void insert_at_position(Node *head, int pos, int val)
{
    Node *newNode = new Node(val);
    Node *tmp = head;
    for (int i = 1; i <= pos - 1; i++)
    {
        tmp = tmp->next;
    }
    newNode->next = tmp->next;
    tmp->next = newNode;
    newNode->prev = tmp;
    newNode->next->prev = newNode;
}

int size(Node *head)
{
    int count = 0;
    Node *tmp = head;
    while (tmp != NULL)
    {
        count++;
        tmp = tmp->next;
    }
    return count;
}

int main()
{
    Node *head = NULL;
    Node *tail = NULL;
    int q;
    cin >> q;
    while (q--)
    {
        int X, V;
        cin >> X >> V;
        if (X > size(head))
        {
            cout << "Invalid"<<endl;
        }
        else if (X == 0)
        {
            insert_head(head, tail, V);
            cout<<"L -> ";
            print_left_to_right(head);
            cout<<"R -> ";
            print_reverse(tail);
        }
        else if (X == size(head))
        {
            insert_tail(head, tail, V);
            cout<<"L -> ";
            print_left_to_right(head);
            cout<<"R -> ";
            print_reverse(tail);
        }
        else
        {
            insert_at_position(head, X, V);
            cout<<"L -> ";
            print_left_to_right(head);
            cout<<"R -> ";
            print_reverse(tail);
        }
    }
    return 0;
}